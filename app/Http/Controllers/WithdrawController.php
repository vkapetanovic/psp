<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class WithdrawController extends Controller
{
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,id',
            'amount' => 'required|min:1|numeric'
        ]);

        // return errors
        if ($validation->fails())
        {
            return response()
                ->json($validation->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $customer = Customer::findOrFail($request->input('customer_id'));

        // Check if customer is eligible for withdrawal
        if ($customer->isEligibleForWithdraw($request->input('amount')))
        {
            return response()
                ->json(['success' => false, 'message' => 'Insufficient balance.']);
        }

        DB::beginTransaction();

        try
        {
            // add withdraw record
            $withdraw = new Withdraw();
            $withdraw->customer_id = $customer->id;
            $withdraw->amount = $request->input('amount');
            $withdraw->country = $customer->country;
            $withdraw->save();

            DB::commit();

            return response()
                ->json(['success' => true, 'customer' => $customer->refresh()]);
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            \Log::error($e);

            // For testing purpose show the exception error. Otherwise we would show custom error message not
            // reveling all the details about the error (like getMessage() method does)
            return response()
                ->json($validation->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

    }
}
