<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Deposit;
use App\Models\Report;
use App\Models\Withdraw;
use App\Services\ReportService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->input('from')) {
            $from = Carbon::parse($request->input('from'))->startOfDay();
        } else {
            $from = Carbon::now()->subDays(7)->startOfDay();
        }

        if ($request->input('to')) {
            $to = Carbon::parse($request->input('to'))->endOfDay();
        } else {
            $to = Carbon::now()->endOfDay();
        }

        $reportService = new ReportService();
        $dailyReport = $reportService->reportPerCountries($from, $to);

        return response()->json(['report' => $dailyReport]);


    }
}
