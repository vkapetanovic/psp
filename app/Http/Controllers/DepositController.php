<?php

namespace App\Http\Controllers;

use App\Models\Deposit;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;

class DepositController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,id',
            'amount' => 'required|min:1|numeric'
        ]);

        // return errors
        if ($validation->fails())
        {
            return response()
                ->json($validation->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }


        // Using transaction we are implementing the "pessimistic approach" database locking
        DB::beginTransaction();

        try
        {
            $customer = Customer::findOrFail($request->input('customer_id'));

            // make deposit
            $deposit = new Deposit();
            $deposit->customer_id = $customer->id;
            $deposit->amount = $request->input('amount');
            $deposit->bonus_amount = $deposit->calculateBonusAmount($customer->bonus);
            $deposit->country = $customer->country;
            $deposit->save();

            DB::commit();

            return response()
                ->json(['success' => true, 'customer' => $customer->refresh()]);
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            \Log::error($e);

            // For testing purpose show the exception error. Otherwise we would show custom error message not
            // reveling all the details about the error (like getMessage() method does)
            return response()
                ->json($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

    }
}
