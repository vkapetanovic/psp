<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required|in:f,m',
            'email' => 'required|email|unique:customers',
            'country' => 'required'
        ], [
            'gender.in' => 'Set gender to f or m.'
        ]);

        // return validation errors
        if ($validation->fails())
        {
            return response()->json($validation->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try
        {
            $customer = new Customer();
            $customer->first_name = $request->input('first_name');
            $customer->last_name = $request->input('last_name');
            $customer->email = $request->input('email');
            $customer->gender = $request->input('gender');
            $customer->country = $request->input('country');
            $customer->bonus = rand(5, 20);
            $customer->save();

            return response()
                ->json(['customer' => $customer]);
        }
        catch (\Exception $e)
        {
            return response()
                ->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);

        return response()->json($customer);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);

        $validation = Validator::make($request->all(), [
            'gender' => 'nullable|in:f,m',
            'email' => "nullable|email|unique:customers,email,$customer->id",
        ]);

        // return errors
        if ($validation->fails())
        {
            return response()
                ->json($validation->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try
        {
            if ($request->input('first_name'))
            {
                $customer->first_name = $request->input('first_name');
            }
            if ($request->input('last_name'))
            {
                $customer->last_name = $request->input('last_name');
            }
            if ($request->input('country'))
            {
                $customer->country = $request->input('country');
            }
            if ($request->input('email'))
            {
                $customer->email = $request->input('email');
            }
            if ($request->input('gender'))
            {
                $customer->gender = $request->input('gender');
            }

            $customer->save();

            return response()
                ->json(['customer' => $customer]);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()
                ->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
