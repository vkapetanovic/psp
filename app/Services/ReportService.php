<?php

namespace App\Services;

use App\Models\Deposit;
use App\Models\Withdraw;
use Illuminate\Database\Eloquent\Collection;

class ReportService
{
    public function reportPerCountries($from, $to)
    {
        $dailyPerCountryReport = new Collection();

        $withdrawals = Withdraw::with('customer')->whereBetween('created_at', [$from, $to])->get();
        $deposits = Deposit::with('customer')->whereBetween('created_at', [$from, $to])->get();

        $today = clone $from;

        while ($today <= $to) {

            $todayWithdrawals = $withdrawals->filter(function ($withdrawal) use ($today) {
                return $withdrawal->created_at >= $today->startOfDay() && $withdrawal->created_at <= $today->endOfDay();
            });
            $todayDeposits = $deposits->filter(function ($deposit) use ($today) {
                return $deposit->created_at >= $today->startOfDay() && $deposit->created_at <= $today->endOfDay();
            });

            $countries = $todayWithdrawals->pluck('country')->merge($todayDeposits->pluck('country'))->unique();
            foreach ($countries as $country) {
                $data = [];
                $unique_customers = $todayWithdrawals->where('country', $country)->pluck('customer_id')->merge($todayDeposits->where('country', $country)->pluck('customer_id'))->unique();

                $data['data'] = $today->toDateString();
                $data['country'] = $country;
                $data['unique_customers'] = $unique_customers->count();
                $data['deposits_number'] = $todayDeposits->where('country', $country)->count();
                $data['total_deposit'] = $todayDeposits->where('country', $country)->sum('amount');
                $data['withdrawals_number'] = $todayWithdrawals->where('country', $country)->count();
                $data['total_withdrawals'] = $todayWithdrawals->where('country', $country)->sum('amount');


                $dailyPerCountryReport->add($data);
            }
            $today->addDay();
        }

        return $dailyPerCountryReport;
    }
}
