<?php

namespace App\Models;

use App\Models\Deposit;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function deposits()
    {
        return $this->hasMany(Deposit::class);
    }

    public function withdraws()
    {
        return $this->hasMany(Withdraw::class);
    }

    /**
     * @param $balance
     * @return mixed
     */
    public function increaseBalance($amount, $bonusBalance)
    {
        $this->balance += $amount;
        $this->bonus_balance += $bonusBalance;
    }

    /**
     * @param $amount
     */
    public function decreaseBalance($amount)
    {
        $this->balance -= $amount;
    }

    /**
     * @param $amount
     * @return bool
     */
    public function isEligibleForWithdraw($amount)
    {
        return $this->balance == 0 || $this->balance - $amount < 0;
    }
}
