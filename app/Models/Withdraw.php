<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    static function boot()
    {
        self::created(function ($withdraw) {
            $customer = $withdraw->customer;
            if ($customer) {
                $customer->decreaseBalance($withdraw->amount);
                $customer->save();
            }
        });
        parent::boot();
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
