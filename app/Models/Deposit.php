<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    static function boot()
    {
        self::created(function ($deposit) {
            $customer = $deposit->customer;
            if ($customer) {
                $customer->increaseBalance($deposit->amount, $deposit->bonus_amount);
                $customer->save();

            }
        });
        parent::boot();
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * Check if customer is eligible for bonus. Increase by one as we want bonus for this (unsaved) deposit
     *
     * @return bool
     */
    private function _eligibleForBonus()
    {
        return ($this->customer->deposits->count() + 1) % 3 === 0;
    }

    /**
     * Calculate bonus amount
     *
     * @return float|int
     */
    public function calculateBonusAmount($customerBonus)
    {
        return $this->_eligibleForBonus() ? ($customerBonus / $this->amount) * 100 : 0;
    }
}
