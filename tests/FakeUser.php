<?php
namespace Tests;


use App\Models\Customer;
use Illuminate\Support\Str;

class FakeUser
{
    /**
     * Create a dummy customer
     *
     * @return array
     */
    public function getCustomer()
    {
        return [
            'first_name' => 'Vedran',
            'last_name' => 'Kapetanovic',
            'gender' => 'f',
            'email' => Str::random(6).'-fake-email@email.com',
            'country' => 'BA'
        ];
    }

    /**
     * Take existing customer
     *
     * @return mixed
     */
    public function getExistingCustomer()
    {
        $totalUsers = Customer::count();

        $id = rand(1, $totalUsers);

        $customer = Customer::find($id);

        return $customer->toArray();
    }
}
