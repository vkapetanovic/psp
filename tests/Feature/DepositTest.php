<?php

namespace Tests\Feature;

use App\Models\Customer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DepositTest extends TestCase
{
    const URI = '/api/v1/deposit';

    private $deposit = [
        'customer_id' => 1,
        'amount' => 100,
    ];

    public function testAddDeposit()
    {
        $response = $this->call('POST', self::URI, $this->deposit);

        $this->assertEquals(200, $response->getStatusCode());
        $response->assertJsonStructure([
            'success'
        ]);
    }
}
