<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\FakeUser;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    const URI = '/api/v1/customers';

    public function testCreateCustomer()
    {
        // make a fake user
        $fakeUser = (new FakeUser())->getCustomer();

        // call
        $response = $this->call('POST', self::URI, $fakeUser);
        $data = json_decode($response->getContent(), true);

        // test
        $this->assertEquals(200, $response->getStatusCode());
        $response->assertJsonStructure([
            'customer' => [
                'id'
            ],
        ]);
    }

    public function testExistingEmail()
    {
        // make a fake user
        $fakeUser = (new FakeUser())->getExistingCustomer();

        // call
        $response = $this->call('POST', self::URI, $fakeUser);

        // test
        $this->assertJsonStringEqualsJsonString('{"email":["The email has already been taken."]}', trim($response->getContent()));
        $this->assertEquals(422, $response->getStatusCode());
    }

    public function testUpdatedCustomer()
    {
        // make a fake user
        $fakeUser = (new FakeUser())->getCustomer();

        // create new user
        $createResponse = $this->call('POST', self::URI, $fakeUser);
        $data = json_decode($createResponse->getContent(), true);

        // no need to test createResponse as it was tested in previous method @testCreateCustomer

        // create an update
        $newName = 'fake-name-'.time();
        $fakeUser['first_name'] = $newName;
        $updateResponse = $this->call('PUT', implode('/', [self::URI, $data['customer']['id']]) , $fakeUser);
        $data = json_decode($updateResponse->getContent(), true);

        $this->assertEquals($newName, $data['customer']['first_name']);
        $this->assertEquals(200, $updateResponse->getStatusCode());
    }
}
