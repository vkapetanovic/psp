<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix' => 'v1'], function () use ($router) {
    Route::get('/', function () use ($router) {
        return 'v1/api';
    });

    Route::apiResource('customers', 'CustomerController');
    Route::apiResource('deposit', 'DepositController');
    Route::apiResource('withdraw', 'WithdrawController');
    Route::apiResource('report', 'ReportController');
});