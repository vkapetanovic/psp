## How to install
1. Clone this repository
1. run `composer install`
1. Set environment for production `.env` and testing `.env.testing`
    1. If necessary run `php artisan key:generate`
1. Run migrations `php artisan migrate`

## Routes

1. Customer routes
    1. `POST` `/api/v1/customer` - create new customer
        1. Accepts `first_name` (string), `last_name` (string), `email` (string), `gender` (m/f), `country` (string)
    1. `PUT` `/api/v1/customer/{customer_id}` - update existing customer
    
2. Deposit route
    2. `POST` `/api/v1/deposit`
        2. Accepts `customer_id` (int), `amount` (decimal)
        
3. Withdrawal route
    3. `POST` `/api/v1/withdraw`
        3. Accepts `customer_id` (int), `amount` (decimal)
        
4. Report route
    4. `GET` `/api/v1/report`
        4. Accepts `from` (date), `to` (date)
        
        
## Tests

1. Run unit tests using `phpunit` command in root directory.